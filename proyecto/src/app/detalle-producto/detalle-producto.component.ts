import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css']
})
export class DetalleProductoComponent implements OnInit {

  producto?: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    console.log(this.activatedRoute.snapshot.params);

    //https://reqres.in/api/users/2
    this.httpClient.get(`${environment.apiResPath}/users/$(id)`)
    .subscribe(
      data => (
        this.producto = JSON.stringify(data);
      )
    );
  }
//Revisar el .subscribe por lo que suba el pavo
}
