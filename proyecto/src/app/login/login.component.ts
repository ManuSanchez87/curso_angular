import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})
export class LoginComponent implements OnInit, OnDestroy {
  //Para recibir el valor desde el padre!
  @Input()
  user?: string;

  //Para recibir el valor desde el hijo!
  @Output()
  informarAlPadre = new EventEmitter<string>();

  username?:string;
  password?:string;

  constructor(
    loginService: LoginService
  ) {
    console.log(loginService.getName());
  }

  ngOnInit(): void {
    this.username = this.user;
  }

  ngOnDestroy(): void {
    console.log('Se va a proceder a su destrucción en 3, 2, 1... DESTRUIDO!!!');
  }

  emitirMensaje() {
    //emitimos el mensaje al padre
    this.informarAlPadre.emit('desde el hijo mando:' + this.username);
  }
}
