import { Component } from '@angular/core';
import { LoginService } from '../../login.service';

export interface Lengauaje {
  name: string,
  id: number
}

@Component({
  selector: 'app-root',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent {
  title = 'angular-loyal-v2';
  mostarTexto = true;
  lenguajes:Lengauaje[] = [
    {name: 'java', id:1},
    {name: 'js',id:2},
    {name: 'angular', id:3}
  ];

  showLogin = true;

  constructor(
    private loginService: LoginService
  ) {
    console.log(this.loginService.getName());
  }

  getTitle(): string {
    return this.title;
  }

  cambiar(): void {
    this.mostarTexto = ! this.mostarTexto;
  }

  mostrarLogin(): void {
    this.showLogin = true;
  }
  ocultarLogin() : void {
    this.showLogin = false;
  }

  recibirMensaje(event:any) {
    console.log(event);
  }
}
