import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto, ProductoService } from '../producto.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  productoId?: number;
  id?: number;
  productos: Producto[] = [];

  constructor(
    private productoService: ProductoService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    //snapshot
    this.id = this.activatedRoute.snapshot.params['id'];
    console.log(this.activatedRoute.snapshot.params);

    //observable
    this.productos = this.productoService.getProductos();
  }

  cargarDetalle(id: number): void{
    this.productoId = id;
  }
}
