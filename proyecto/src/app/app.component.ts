import { Component } from '@angular/core';
import { Route, Router } from '@angular/router';
//import { LoginService } from './login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = ' proyecto';

  constructor(
    private router: Router
  ) {

  }
  navegarPorTS(): void{
    // producto/1
    this.router.navigate(['/producto2/1']);
  }
}

